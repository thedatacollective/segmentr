# segmentr

`segmentr` is a set of useful functions and datasets for the creation and segmentation of charity data. It has been developed by [The Data Collective](https://thedatacollective.com.au) as a way to make use of consistent functions across a range of client work.

## Datasets
The following datasets exist within the package. They are all `data.table` objects.

- `lookup_classifications`: a reference table for classification codes
- `lookup_channel`: a reference table for channel codes
- `segments`: a detailed reference table for use with RFV segmentation
- `ask_conversion`: a lookup table to apply ask bandings based on different multipliers

## Functions
- `seg_tenure`:
- `seg_recency`:
- `seg_frequency`:
- `seg_value`:
- `seg_rgvalue`:
- `seg_highvalue`:

This is a basic example which shows you how to solve a common problem:

``` r
## basic example code
```
