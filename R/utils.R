#' Copy to clipboard
#'
#' Copies an object to the clipboard using a tab as separator. Useful if needing
#' to copy data.tables into Excel.
#'
#' At present it is only designed to work on a Mac.
#'
#' @param x a data object (vectors will be copied as a column of data)
#' @param include_header boolean value to include the column names or not.
#' @param remote ssh address of remote host to have the data pasted to
#'
#' @export
#'
#' @examples
#' dt <- data.frame(a = c("A", "B"), b = c(1, 2))
#' copy_clip(dt)
#' copy_clip(dt, include_header = FALSE)
copy_clip <- function(x, include_header = TRUE, remote = NULL) {
  if (is.null(remote)) {
    utils::write.table(x, pipe("pbcopy"), sep = "\t",
                       row.names = FALSE, col.names = include_header)
  } else {
    utils::write.table(x, "tmp_dat", sep = "\t",
                       row.names = FALSE, col.names = include_header)
    system(paste("cat tmp_dat | ssh", remote, "pbcopy"))
    system("rm tmp_dat")
  }
}

#' Paste with NAs
#'
#' Utilises the \link[base]{paste} function to concatenate string values while
#' removing NA values.
#'
#' @param ... one or more R objects, to be converted to character vectors.
#' @param sep the separator to be used between values.
#' @param collapse an optional character string to separate the results.
#'
#' @return a character string
#' @export
#'
#' @examples
#' paste_na("This", NA, "is a string")
paste_na <- function(..., sep=" ", collapse = NULL) {
  L <- list(...)
  L <- lapply(L, function(x) {x[is.na(x)] <- ""; x})
  if (is.null(collapse)) {
    ret <- gsub(paste0("(^", sep ,"|", sep, "$)"), "",
                gsub(paste0(sep, sep), sep,
                     do.call(paste, c(L, list(sep = sep, collapse = collapse)))))
  } else {
    ret <- gsub(paste0("(^", sep ,"|", sep, "$|^", collapse, "|", collapse, "$)"), "",
                gsub(paste0(sep, sep), sep,
                     gsub(paste0(collapse, collapse), collapse,
                          gsub(paste0("(", collapse, sep, "|", sep, collapse, ")"), collapse,
                               do.call(paste, c(L, list(sep = sep, collapse = collapse)))))))
  }
  is.na(ret) <- ret==""
  ret
}

#' Zip File
#'
#' Utilises the \link[base]{paste} function to concatenate string values while
#' removing NA values.
#'
#' @param file the file to be zipped
#' @param path the path to the file
#' @param password an optional password to apply to the zup file
#'
#' @return nothing
#' @export
#'
#' @examples
#' \donttest{
#'   zip_file("test.csv")
#'   zip_file("test.csv", "data_out")
#'   zip_file("test.csv", "data_out", "mypassword")
#' }
zip_file <- function(file, path = ".", password = NULL) {
  if (is.null(file)) stop("No file provided")

  if (!file.exists(paste("path", file, sep = "/"))) stop("File doesn't exist")

  file_part <- sub('\\..*$', '', basename(file))

  if (is.null(password)) {
    cmd <- paste0("pushd ", path, "; zip -r ", file_part, ".zip ", file, "; popd")
  } else {
    cmd <- paste0("pushd ", path, "; zip -P ", password, " -r ", file_part, ".zip ", file, "; popd")
  }

  system("bash", input = cmd)
}
