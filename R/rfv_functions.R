#' Segmentation Recency
#'
#' @description Determine the difference between two dates in 12 month bandings.
#'  (e.g. 0-12, 13-24, 25-36, 37-48, 49-60, 61-72, 73-84, 85+)
#'
#' @param last_gift_date The date of the last gift a person has given
#' @param refresh_date The date of comparison for the recency
#'
#' @return a factor of recency bandings
#' @export
#'
#' @examples
#' seg_recency("2017-09-16", "2018-03-08")
#'
#' dates1 <- c("2016-09-16", "2012-08-12", "2012-09-22", "2018-10-16")
#' seg_recency(dates1, "2018-09-16")
#'
seg_recency <- function(last_gift_date, refresh_date) {
  last_gift_date <- as.Date(last_gift_date)
  refresh_date <- as.Date(refresh_date)

  x <- lubridate::interval(
    last_gift_date,
    refresh_date) / months(1)
  x <- replace(x, is.na(x), -Inf)

  cut(x, breaks = c(-Inf, -0.000001, 12, 24, 36, 48, 60, 72, 84, Inf),
      labels = c("ERROR", "0-12", "13-24", "25-36", "37-48",
                 "49-60", "61-72", "73-84", "85+"),
      include.lowest = T)
}

#' Segmentation Tenure
#'
#' @description Determine the difference between two dates in 12 month bandings.
#'  (e.g. 0-12, 13-24, 25-36, 37-48, 49-60, 61-72, 73-84, 85+)
#'
#' @param first_gift_date The date of the last gift a person has given
#' @param refresh_date The date of comparison for the recency
#'
#' @return a factor of recency bandings
#' @export
#'
#' @examples
#' seg_tenure("2017-09-16", "2018-03-08")
#'
#' dates1 <- c("2016-09-16", "2012-08-12", "2012-09-22", "2018-10-16")
#' seg_tenure(dates1, "2018-09-16")
#'
seg_tenure<- function(first_gift_date, refresh_date) {
  last_gift_date <- as.Date(first_gift_date)
  refresh_date <- as.Date(refresh_date)

  x <- lubridate::`%--%`(first_gift_date, refresh_date) / months(1)
  x <- replace(x, is.na(x), -Inf)

  cut(x, breaks = c(-Inf, -0.000001, 12, 24, 36, 48, 60, 72, 84, Inf),
      labels = c("ERROR", "0-12", "13-24", "25-36", "37-48",
                 "49-60", "61-72", "73-84", "85+"),
      include.lowest = T)
}

#' Segmentation Frequency
#'
#' @param total_gifts an integer value identifying the number of gifts given
#'
#' @return a factor of giving frequencies
#' @export
#'
#' @examples
#' seg_frequency(1)
#' seg_frequency(2)
#' seg_frequency(c(-1, 0, 1, 2, 100))
#' seg_frequency(c(0.5, 1.2, 1.99, 2.0, 2.5))
#'
seg_frequency <- function(total_gifts) {
  total_gifts <- suppressWarnings(as.integer(total_gifts))
  x <- replace(total_gifts, is.na(total_gifts), -Inf)

  out <- cut(x, breaks = c(-Inf, 1, 2, Inf), labels = c("ERROR", "1", ">1"),
             include.lowest = T, right = F)

  factor(out, levels = c(">1", "1", "ERROR"))
}

#' Segmentation Value Band
#'
#' @description Determine the value band for a particular giving amount
#'
#' @param amount a numeric value to be grouped into value bands
#'
#' @return a factor of value bandings
#' @export
#'
#' @examples
#' seg_value(100)
#'
#' values1 <- c(-5, 5, 15, 45,135, 405, 810 ,1215)
#' seg_value(values1)
#'
seg_value <- function(amount) {
  amount <- suppressWarnings(as.numeric(amount))
  x <- replace(amount, is.na(amount), -Inf)

  cut(x, breaks = c(-Inf, 0.00001, 10, 25, 50, 100, 250, 500, 1000, Inf),
      labels = c("ERROR", "<$10", "$10-$24.99", "$25-$49.99", "$50-$99.99",
                 "$100-$249.99", "$250-$499.99", "$500-$999.99", "$1,000+"),
      include.lowest = T, right = F)
}

#' Segmentation Regular Giving Value Band
#'
#' @description Determine the value band for a particular regular giving amount
#'
#' @param amount a numeric value to be grouped into value bands
#'
#' @return a factor of value bandings
#' @export
#'
#' @examples
#' seg_value(100)
#'
#' values1 <- c(-5, 5, 15, 45,135, 405, 810 ,1215)
#' seg_value(values1)
#'
seg_value_rg <- function(amount) {
  amount <- suppressWarnings(as.numeric(amount))
  x <- replace(amount, is.na(amount), -Inf)

  cut(x, breaks = c(-Inf, 0.00001, 10, 20, 30, 40, 60, 200, 600, Inf),
      labels = c("ERROR", "<$10", "$10-$19.99", "$20-$29.99", "$30-$39.99",
                 "$40-$59.99", "$60-$199.99", "$200-$599.99", "$600+"),
      include.lowest = T, right = F)
}

#' Segmentation High Value Gift Value Band
#'
#' @description Determine the value band for a particular high value amount
#'
#' @param amount a numeric value to be grouped into value bands
#'
#' @return a factor of value bandings
#' @export
#'
#' @examples
#' seg_value(100)
#'
#' values1 <- c(-5, 5, 15, 45,135, 405, 810 ,1215)
#' seg_value(values1)
#'
seg_value_hv <- function(amount) {
  amount <- suppressWarnings(as.numeric(amount))
  x <- replace(amount, is.na(amount), -Inf)

  cut(x, breaks = c(-Inf, 0.00001, 100, 250, 500, 1000, 2500, 5000, 10000, 25000,
                    50000, 100000, Inf),
      labels = c("ERROR", "<$100", "$100-$249.99", "$250-$499.99",
                 "$500-$999.99", "$1,000-$2,499.99", "$2,500-$4,999.99",
                 "$5,000-$9,999.99",  "$10,000-$24,999.99",
                 "$25,000-$49,999.99", "$50,000-$99,999.99", "$100,000+"),
      include.lowest = T, right = F)
}

