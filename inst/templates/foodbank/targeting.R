library(data.table)
library(lubridate)
library(segmentr)
library(readxl)

project_code <- "TDC00001"
remote <- "dwilson@192.168.2.100"

# Import data -------------------------------------------------------------
load("../standard_refresh/data/donors.Rda")
load("../standard_refresh/data/trans.Rda")
load("../standard_refresh/data/rgtable.Rda")
load("../standard_refresh/data/exclusions.Rda")

# Apply exclusions --------------------------------------------------------
# populate data brief (Exclusions) with possible exclusion codes
exclusions[, .N, .(group, exclusion_code, description)] %>%
  copy_clip(include_header = FALSE, remote = remote)

donors[, `:=`(remove_all = NA_character_, remove_w1 = NA_character_,
              remove_w2 = NA_character_, remove_w3 = NA_character_)]

# import data exclusions
brief <- list.files("brief", pattern = "data_brief.*xlsx$", full.names = TRUE)
appeal_excl <- read_excel(brief[length(brief)], "Exclusions", skip = 3)
setDT(appeal_excl)

exclusions[appeal_excl, on = c("exclusion_code", "description"),
           `:=`(remove_all = i.remove_all, remove_w1 = i.remove_w1,
                remove_w2 = i.remove_w2, remove_w3 = i.remove_w3)]

# remove records from all waves
exclude <- exclusions[remove_all == "Y", .SD[1], keyby = urn]
donors[exclude, on = "urn",
       `:=`(remove_all = ifelse(is.na(remove_all),
                                paste(exclusion_code, description, sep = "-"),
                                remove_all))]

# remove records from wave 1
exclude <- exclusions[remove_all == "Y" | remove_w1 == "Y", .SD[1], keyby = urn]
donors[exclude, on = "urn",
       `:=`(remove_w1 = ifelse(is.na(remove_w1),
                               paste(exclusion_code, description, sep = "-"),
                               remove_w1))]

# remove records from wave 2
exclude <- exclusions[remove_all == "Y" | remove_w2 == "Y", .SD[1], keyby = urn]
donors[exclude, on = "urn",
       `:=`(remove_w2 = ifelse(is.na(remove_w2),
                                paste(exclusion_code, description, sep = "-"),
                               remove_w2))]

# remove records from wave 3
exclude <- exclusions[remove_all == "Y" | remove_w3 == "Y", .SD[1], keyby = urn]
donors[exclude, on = "urn",
       `:=`(remove_w3 = ifelse(is.na(remove_w3),
                                paste(exclusion_code, description, sep = "-"),
                               remove_w3))]

# build exclusion counts for brief
tmp_excl <- appeal_excl[, 1:3]
tmp_remove <- donors[, .N, (exclusion_code = as.numeric(substr(remove_all, 1, 3)))]
tmp_excl[tmp_remove, on = "exclusion_code", remove_all := N]
tmp_remove <- donors[, .N, (exclusion_code = as.numeric(substr(remove_w1, 1, 3)))]
tmp_excl[tmp_remove, on = "exclusion_code", remove_w1 := N]
tmp_remove <- donors[, .N, (exclusion_code = as.numeric(substr(remove_w2, 1, 3)))]
tmp_excl[tmp_remove, on = "exclusion_code", remove_w2 := N]
tmp_remove <- donors[, .N, (exclusion_code = as.numeric(substr(remove_w3, 1, 3)))]
tmp_excl[tmp_remove, on = "exclusion_code", remove_w3 := N]
tmp_excl[, .(remove_all, remove_w1, remove_w2, remove_w3)] %>%
  copy_clip(include_header = FALSE, remote = remote)
rm(tmp_excl, tmp_remove)

# Identify available mail counts ------------------------------------------

# available donor w1
donors[is.na(remove_w1), .N, keyby = segment_code] %>%
  copy_clip(remote = remote)

# available donors w2
segment_counts <- donors[is.na(remove_w2), .N, keyby = segment_code] %>%
  copy_clip(remote = remote)

# available donors w3
segment_counts <- donors[is.na(remove_w3), .N, keyby = segment_code] %>%
  copy_clip(remote = remote)