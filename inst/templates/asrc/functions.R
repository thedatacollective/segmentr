
get_recency <- function(last_gift_date, refresh_date) {
  last_gift_date <- as.Date(last_gift_date)
  refresh_date <- as.Date(refresh_date)
  
  x <- (as.yearmon(refresh_date)-as.yearmon(last_gift_date))*12
  
  x <- replace(x, is.na(x), -Inf)
  
  cut(x, breaks = c(-Inf, -0.000001, 12, 24, 36, 48, 60, 72, 84, Inf), 
      labels = c("ERROR", "0-12", "13-24", "25-36", "37-48",
                 "49-60", "61-72", "73-84", "85+"),
      include.lowest = T)
}

get_tenure <- function(first_gift_date, last_gift_date) {
  # create test for presence of both dates
  first_gift_date <- as.Date(first_gift_date)
  last_gift_date <- as.Date(last_gift_date)
  
  x <- (as.yearmon(last_gift_date)-as.yearmon(first_gift_date))*12
  
  x <- replace(x, is.na(x), -Inf)
  
  cut(x, breaks = c(-Inf, -0.000001, 12, 24, 36, 48, 60, 72, 84, Inf), 
      labels = c("ERROR", "0-12", "13-24", "25-36", "37-48", 
                 "49-60", "61-72", "73-84", "85+"), 
      include.lowest = T)
}

get_value <- function(amount) {
  x <- replace(amount, is.na(amount), -Inf)
  
  cut(x, breaks = c(-Inf, 0, 10, 25, 50, 100, 250, 500, 1000, Inf),
      labels = c("ERROR", "<$10", "$10-$24.99", "$25-$49.99", "$50-$99.99",
                 "$100-$249.99", "$250-$499.99", "$500-$999.99", "$1,000+"),
      include.lowest = T, right = F)
}

get_frequency <- function(total_gifts) {
  x <- replace(total_gifts, is.na(total_gifts), -Inf)

  cut(x, breaks = c(-Inf, 1, 2, Inf),
      labels = c("ERROR", "1", ">1"), right = F)
}

get_highvalue <- function(amount) {
  x <- replace(amount, is.na(amount), -Inf)
  
  cut(x, breaks = c(-Inf, 0, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 
                    50000, 100000, Inf),
      labels = c("ERROR", "<$100", "$100-$249.99", "$250-$499.99", "$500-$999.99",
                 "$1,000-$2,499.99", "$2,500-$4,999.99", "$5,000-$9,999.99", 
                 "$10,000-$24,999.99", "$25,000-$49,999.99", "$50,000-$99,999.99",
                 "$100,000+"),
      include.lowest = T, right = F)
}

dtcalc_trans <- function(x, classification_codes = NA, channel_codes = NA, prefix = "") {
  if (!is.na(classification_codes)) {
    x <- x[classification_code %in% classification_codes]
  }
  
  if (!is.na(channel_codes)) {
    x <- x [channel_code %in% c(channel_codes)]
  }
  
  if (prefix != "") 
    prefix <- paste0(prefix, "_")
  
  f_data <- x[x[, .I[1], urn]$V1, setNames(
    list(date, amount, appeal_code),
    paste0(prefix, c("first_gift_date", "first_amount", "first_appeal"))), urn]
  
  l_data <- x[x[, .I[.N], urn]$V1, setNames(
    list(date, amount, appeal_code),
    paste0(prefix, c("last_gift_date", "last_amount", "last_appeal"))), urn]
  
  fl_data <- f_data[l_data, on = "urn"]
  
  y <- x[, setNames(list(1, max(amount), mean(amount), .N, sum(amount)),
                    paste0(prefix, c("flag", "max_amount", "avg_amount", 
                                     "total_gifts", "total_income"))),
         by = urn]
  
  y[fl_data, on = "urn"]
  
}





get_value <- function(amount) {
  x <- replace(amount, is.na(amount), -Inf)
  
  cut(x, breaks = c(-Inf, 0, 10, 25, 50, 100, 250, 500, 1000, Inf),
      labels = c("ERROR", "<$10", "$10-$24.99", "$25-$49.99", "$50-$99.99",
                 "$100-$249.99", "$250-$499.99", "$500-$999.99", "$1,000+"),
      include.lowest = T, right = F)
}



calc_rfvt <- function(x, refresh_date, group = "financial") {
  lgd <- paste0(group, "_last_gift_date")
  tg <-  paste0(group, "_total_gifts")
  mv <-  paste0(group, "_max_amount")
  fgd <- paste0(group, "_first_gift_date")
  
  x[, c(paste0(group, c("_recency", "_frequency", "_valueband", "_tenure"))) := 
      .(seg_recency(get(lgd), refresh_date), 
        seg_frequency(get(tg)),
        seg_value(get(mv)),
        seg_tenure(get(fgd), refresh_date))]
  
}

calc_active <- function(x, classification_codes = NA, prefix = "") {
  if (length(classification_codes) > 0) {
    x <- x[rg_classification_code %in% classification_codes]
  }
  
  if (prefix != "")
    prefix <- paste0(prefix, "_")
  
  x[tolower(status) == "active",
    setNames(list(1), paste0(prefix, "active_flag")),
    by = .(urn)]
}

calc_donors <- function(x, x1 = donors, x2 = trans, x3 = rgtable, x4 = segments,
                        start_date, verbose = FALSE) {
  
  x_urn <- get(x)$urn
  
  if (verbose) message(Sys.time(), " - Calculating RFV for ", x)
  
  # truncate data
  donors <- x1[urn %in% x_urn, 
               .(urn, donor_category, title, firstname, surname, addr_line1, 
                 addr_line2, addr_line3, addr_suburb, addr_state, addr_postcode, 
                 phone_home, phone_work, phone_mobile, email, demog_gender, 
                 demog_dob, beq_confirmed)]
  trans <- x2[urn %in% x_urn & date < start_date]
  rgtable <- x3[urn %in% x_urn & first_gift_date < start_date]
  # rgtable[terminated_date > start_date, status := "ACTIVE"]
  segments <- x4
  refresh_date <- as.Date(start_date, origin = "1970-01-01")  - 1
  message("Refresh Date is: ", refresh_date)
  
  if (verbose) message(Sys.time(), " - Data truncated")
  
  # identify gift types
  if (verbose) message(Sys.time(), " - Calculating CASH trans")
  ct <- dtcalc_trans(trans, 5, prefix = "cash")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating RG trans")
  ct <- dtcalc_trans(trans, 1, prefix = "rg")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating LOTTERY trans")
  ct <- dtcalc_trans(trans, 11, prefix = "lottery")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating CSPON trans")
  ct <- dtcalc_trans(trans, 33, prefix = "cspon")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating BEQUEST trans")
  ct <- dtcalc_trans(trans, 2, prefix = "bequest")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating EVENT trans")
  ct <- dtcalc_trans(trans, c(3,7), prefix = "event")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating EMERGENCY trans")
  ct <- dtcalc_trans(trans, c(40,60), prefix = "emergency")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating MEMBER trans")
  ct <- dtcalc_trans(trans, 12, prefix = "member")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating INMEM trans")
  ct <- dtcalc_trans(trans, 9, prefix = "inmem")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating MERCHANDISE trans")
  ct <- dtcalc_trans(trans, 13, prefix = "merchandise")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating FINANCIAL trans")
  ct <- dtcalc_trans(trans, prefix = "financial")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  # replace NA values for _flag columns
  flag_cols <- colnames(donors)[colnames(donors) %like% "_flag"]
  for (col in flag_cols) donors[is.na(get(col)), (col) := 0]
  
  # create RFVT 
  if (verbose) message(Sys.time(), " - Calculating RFV")
  donors <- calc_rfvt(donors, refresh_date, "cash")
  donors <- calc_rfvt(donors, refresh_date, "rg")
  donors <- calc_rfvt(donors, refresh_date, "emergency")
  donors <- calc_rfvt(donors, refresh_date, "member")
  donors <- calc_rfvt(donors, refresh_date, "merchandise")
  donors <- calc_rfvt(donors, refresh_date, "inmem")
  donors <- calc_rfvt(donors, refresh_date, "event")
  donors <- calc_rfvt(donors, refresh_date, "cspon")
  donors <- calc_rfvt(donors, refresh_date, "lottery")
  
  # create extra flags (rg_active, rg_upgrade, rg_f2f)
  if (verbose) message(Sys.time(), " - Calculating flags")
  donors[as.data.table(calc_active(rgtable, 1, "rg")), on = "urn", 
         rg_active_flag := i.rg_active_flag]
  donors[rg_flag == 1 & is.na(rg_active_flag), rg_active_flag := 0]
  
  # donors[as.data.table(calc_active(rgtable, 33, "cspon")), on = "urn", 
  #        cspon_active_flag := i.cspon_active_flag]
  # donors[cspon_flag == 1 & is.na(cspon_active_flag), cspon_active_flag := 0]
  
  donors[as.data.table(calc_active(rgtable, 3, "member")), on = "urn", 
         member_active_flag := i.member_active_flag]
  donors[member_flag == 1 & is.na(member_active_flag), member_active_flag := 0]
  
  donors[rg_flag == 1 & rg_last_amount > rg_first_amount, rg_upgrade_flag := 1]
  
  tmp_f2f <- trans[gift_num == 1 & classification_code == 1 & channel_code == 10,
                   .(rg_f2f_flag = 1),
                   by = urn]
  donors[tmp_f2f, on = "urn", rg_f2f_flag := i.rg_f2f_flag]
  donors[rg_flag == 1 & is.na(rg_f2f_flag), rg_f2f_flag := 0]
  
  # Identify donor type
  if (verbose) message(Sys.time(), " - Calculating donor_type")
  donors[(rg_flag == 1 | rg_active_flag == 1) & cash_flag == 0, donor_type := "rg"]
  donors[is.na(donor_type) & (rg_flag == 1 | rg_active_flag == 1) & cash_flag == 1,
         donor_type := "rgcash"]
  donors[is.na(donor_type) & cash_flag == 1, donor_type := "cash"]
  donors[is.na(donor_type) & emergency_flag == 1, donor_type := "emergency"]
  donors[is.na(donor_type) & member_flag == 1 & member_active_flag == 1, 
         donor_type := "activemember"]
  donors[is.na(donor_type) & member_flag == 1 & member_active_flag == 0, 
         donor_type := "lapsedmember"]
  donors[is.na(donor_type) & member_flag == 1 & member_active_flag == 1, 
         donor_type := "activemember"]
  donors[is.na(donor_type) & event_flag == 1, donor_type := "event"]
  donors[is.na(donor_type) & lottery_flag == 1, donor_type := "lottery"]
  donors[is.na(donor_type) & merchandise_flag == 1, donor_type := "merchandise"]
  donors[is.na(donor_type) & inmem_flag == 1, donor_type := "inmem"]
  donors[is.na(donor_type) & financial_flag == 1, donor_type := "other"]
  donors[is.na(donor_type), donor_type := "nondonor"]
  
  # apply segmentation
  if (verbose) message(Sys.time(), " - Applying segmentation")
  tmp_rg_segments <- segments[donor_type == "rg"]
  donors[tmp_rg_segments, on = c("donor_type"     = "donor_type", 
                                 "rg_tenure"      = "tenure", 
                                 "rg_f2f_flag"    = "face_to_face", 
                                 "rg_active_flag" = "active_rg"),
         segment_code := i.seg_code]
  
  tmp_rgcash_segments <- segments[donor_type == "rgcash"]
  donors[tmp_rgcash_segments, on = c("donor_type"     = "donor_type", 
                                     "cash_recency"   = "recency", 
                                     "cash_frequency" = "frequency", 
                                     "cash_valueband" = "value",
                                     "rg_active_flag" = "active_rg"),
         segment_code := i.seg_code]
  
  tmp_cash_segments <- segments[donor_type == "cash"]
  donors[tmp_cash_segments, on = c("donor_type"     = "donor_type", 
                                   "cash_recency"   = "recency", 
                                   "cash_frequency" = "frequency", 
                                   "cash_valueband" = "value"),
         segment_code := i.seg_code]
  
  tmp_emerg_segments <- segments[donor_type == "emergency"]
  donors[tmp_emerg_segments, on = c("donor_type"          = "donor_type", 
                                    "emergency_recency"   = "recency", 
                                    "emergency_frequency" = "frequency", 
                                    "emergency_valueband" = "value"),
         segment_code := i.seg_code]
  
  tmp_member_segments <- segments[donor_type %in% c("activemember", "lapsedmember")]
  donors[tmp_member_segments, on = c("donor_type"     = "donor_type", 
                                     "member_recency" = "recency"),
         segment_code := i.seg_code]
  
  tmp_merch_segments <- segments[donor_type == "merchandise"]
  donors[tmp_merch_segments, on = c("donor_type"          = "donor_type", 
                                    "merchandise_recency" = "recency"),
         segment_code := i.seg_code]
  
  tmp_inmem_segments <- segments[donor_type == "inmem"]
  donors[tmp_inmem_segments, on = c("donor_type"    = "donor_type", 
                                    "inmem_recency" = "recency"),
         segment_code := i.seg_code]
  
  tmp_lottery_segments <- segments[donor_type == "lottery"]
  donors[tmp_lottery_segments, on = c("donor_type"        = "donor_type", 
                                      "lottery_recency"   = "recency",
                                      "lottery_frequency" = "frequency",
                                      "lottery_valueband" = "value"),
         segment_code := i.seg_code]
  
  tmp_event_segments <- segments[donor_type == "event"]
  donors[tmp_event_segments, on = c("donor_type"    = "donor_type", 
                                    "event_recency" = "recency"),
         segment_code := i.seg_code]
  
  donors[donor_type == "other", segment_code := "N001"]
  donors[donor_type == "nondonor", segment_code := "P001"]
  
  donors[, .(urn, donor_type, segment_code, appeal = x)]
}

calc_all_donors <- function(x, x1 = donors, x2 = trans, x3 = rgtable, x4 = segments,
                        start_date, verbose = FALSE) {
  
  if (verbose) message(Sys.time(), " - Calculating RFV for ", x)
  
  # truncate data
  donors <- x1[, 
               .(urn, donor_category, title, firstname, surname, addr_line1, 
                 addr_line2, addr_line3, addr_suburb, addr_state, addr_postcode, 
                 phone_home, phone_work, phone_mobile, email, demog_gender, 
                 demog_dob, beq_confirmed)]
  trans <- x2[date < start_date]
  rgtable <- x3[first_gift_date < start_date]
  rgtable[terminated_date > start_date, status := "ACTIVE"]
  segments <- x4
  refresh_date <- as.Date(start_date, origin = "1970-01-01")  - 1
  message("Refresh Date is: ", refresh_date)
  
  if (verbose) message(Sys.time(), " - Data truncated")
  
  # identify gift types
  if (verbose) message(Sys.time(), " - Calculating CASH trans")
  ct <- dtcalc_trans(trans, 5, prefix = "cash")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating RG trans")
  ct <- dtcalc_trans(trans, 1, prefix = "rg")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating LOTTERY trans")
  ct <- dtcalc_trans(trans, 11, prefix = "lottery")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating CSPON trans")
  ct <- dtcalc_trans(trans, 33, prefix = "cspon")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating BEQUEST trans")
  ct <- dtcalc_trans(trans, 2, prefix = "bequest")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating EVENT trans")
  ct <- dtcalc_trans(trans, c(3,7), prefix = "event")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating EMERGENCY trans")
  ct <- dtcalc_trans(trans, c(40,60), prefix = "emergency")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating MEMBER trans")
  ct <- dtcalc_trans(trans, 12, prefix = "member")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating INMEM trans")
  ct <- dtcalc_trans(trans, 9, prefix = "inmem")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating MERCHANDISE trans")
  ct <- dtcalc_trans(trans, 13, prefix = "merchandise")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  if (verbose) message(Sys.time(), " - Calculating FINANCIAL trans")
  ct <- dtcalc_trans(trans, prefix = "financial")
  donors[ct, on = "urn", names(ct)[-1] := mget(paste0("i.", names(ct)[-1]))]
  
  # replace NA values for _flag columns
  flag_cols <- colnames(donors)[colnames(donors) %like% "_flag"]
  for (col in flag_cols) donors[is.na(get(col)), (col) := 0]
  
  # create RFVT 
  if (verbose) message(Sys.time(), " - Calculating RFV")
  donors <- calc_rfvt(donors, refresh_date, "cash")
  donors <- calc_rfvt(donors, refresh_date, "rg")
  donors <- calc_rfvt(donors, refresh_date, "emergency")
  donors <- calc_rfvt(donors, refresh_date, "member")
  donors <- calc_rfvt(donors, refresh_date, "merchandise")
  donors <- calc_rfvt(donors, refresh_date, "inmem")
  donors <- calc_rfvt(donors, refresh_date, "event")
  donors <- calc_rfvt(donors, refresh_date, "cspon")
  donors <- calc_rfvt(donors, refresh_date, "lottery")
  
  # create extra flags (rg_active, rg_upgrade, rg_f2f)
  if (verbose) message(Sys.time(), " - Calculating flags")
  donors[as.data.table(calc_active(rgtable, 1, "rg")), on = "urn", 
         rg_active_flag := i.rg_active_flag]
  donors[rg_flag == 1 & is.na(rg_active_flag), rg_active_flag := 0]
  
  donors[as.data.table(calc_active(rgtable, 33, "cspon")), on = "urn", 
         cspon_active_flag := i.cspon_active_flag]
  donors[cspon_flag == 1 & is.na(cspon_active_flag), cspon_active_flag := 0]
  
  donors[as.data.table(calc_active(rgtable, 3, "member")), on = "urn", 
         member_active_flag := i.member_active_flag]
  donors[member_flag == 1 & is.na(member_active_flag), member_active_flag := 0]
  
  donors[rg_flag == 1 & rg_last_amount > rg_first_amount, rg_upgrade_flag := 1]
  
  tmp_f2f <- trans[gift_num == 1 & classification_code == 1 & channel_code == 10,
                   .(rg_f2f_flag = 1),
                   by = urn]
  donors[tmp_f2f, on = "urn", rg_f2f_flag := i.rg_f2f_flag]
  donors[rg_flag == 1 & is.na(rg_f2f_flag), rg_f2f_flag := 0]
  
  # Identify donor type
  if (verbose) message(Sys.time(), " - Calculating donor_type")
  donors[(rg_flag == 1 | rg_active_flag == 1) & cash_flag == 0, donor_type := "rg"]
  donors[is.na(donor_type) & (rg_flag == 1 | rg_active_flag == 1) & cash_flag == 1,
         donor_type := "rgcash"]
  donors[is.na(donor_type) & cash_flag == 1, donor_type := "cash"]
  donors[is.na(donor_type) & emergency_flag == 1, donor_type := "emergency"]
  donors[is.na(donor_type) & member_flag == 1 & member_active_flag == 1, 
         donor_type := "activemember"]
  donors[is.na(donor_type) & member_flag == 1 & member_active_flag == 0, 
         donor_type := "lapsedmember"]
  donors[is.na(donor_type) & member_flag == 1 & member_active_flag == 1, 
         donor_type := "activemember"]
  donors[is.na(donor_type) & event_flag == 1, donor_type := "event"]
  donors[is.na(donor_type) & lottery_flag == 1, donor_type := "lottery"]
  donors[is.na(donor_type) & merchandise_flag == 1, donor_type := "merchandise"]
  donors[is.na(donor_type) & inmem_flag == 1, donor_type := "inmem"]
  donors[is.na(donor_type) & financial_flag == 1, donor_type := "other"]
  donors[is.na(donor_type), donor_type := "nondonor"]
  
  # apply segmentation
  if (verbose) message(Sys.time(), " - Applying segmentation")
  tmp_rg_segments <- segments[donor_type == "rg"]
  donors[tmp_rg_segments, on = c("donor_type"     = "donor_type", 
                                 "rg_tenure"      = "tenure", 
                                 "rg_f2f_flag"    = "face_to_face", 
                                 "rg_active_flag" = "active_rg"),
         segment_code := i.seg_code]
  
  tmp_rgcash_segments <- segments[donor_type == "rgcash"]
  donors[tmp_rgcash_segments, on = c("donor_type"     = "donor_type", 
                                     "cash_recency"   = "recency", 
                                     "cash_frequency" = "frequency", 
                                     "cash_valueband" = "value",
                                     "rg_active_flag" = "active_rg"),
         segment_code := i.seg_code]
  
  tmp_cash_segments <- segments[donor_type == "cash"]
  donors[tmp_cash_segments, on = c("donor_type"     = "donor_type", 
                                   "cash_recency"   = "recency", 
                                   "cash_frequency" = "frequency", 
                                   "cash_valueband" = "value"),
         segment_code := i.seg_code]
  
  tmp_emerg_segments <- segments[donor_type == "emergency"]
  donors[tmp_emerg_segments, on = c("donor_type"          = "donor_type", 
                                    "emergency_recency"   = "recency", 
                                    "emergency_frequency" = "frequency", 
                                    "emergency_valueband" = "value"),
         segment_code := i.seg_code]
  
  tmp_member_segments <- segments[donor_type %in% c("activemember", "lapsedmember")]
  donors[tmp_member_segments, on = c("donor_type"     = "donor_type", 
                                     "member_recency" = "recency"),
         segment_code := i.seg_code]
  
  tmp_merch_segments <- segments[donor_type == "merchandise"]
  donors[tmp_merch_segments, on = c("donor_type"          = "donor_type", 
                                    "merchandise_recency" = "recency"),
         segment_code := i.seg_code]
  
  tmp_inmem_segments <- segments[donor_type == "inmem"]
  donors[tmp_inmem_segments, on = c("donor_type"    = "donor_type", 
                                    "inmem_recency" = "recency"),
         segment_code := i.seg_code]
  
  tmp_lottery_segments <- segments[donor_type == "lottery"]
  donors[tmp_lottery_segments, on = c("donor_type"        = "donor_type", 
                                      "lottery_recency"   = "recency",
                                      "lottery_frequency" = "frequency",
                                      "lottery_valueband" = "value"),
         segment_code := i.seg_code]
  
  tmp_event_segments <- segments[donor_type == "event"]
  donors[tmp_event_segments, on = c("donor_type"    = "donor_type", 
                                    "event_recency" = "recency"),
         segment_code := i.seg_code]
  
  donors[donor_type == "other", segment_code := "N001"]
  donors[donor_type == "nondonor", segment_code := "P001"]
  
  donors[donor_type != "nondonor", .(urn, donor_type, segment_code, appeal = x)]
}

# calculate exclusions
calc_exclusions <- function(x, x1 = exclusions, x2 = excl_criteria, 
                            start_date, verbose = FALSE) {
  
  if (verbose) message(Sys.time(), " - Calculating exclusions for ", x)
  
  # truncate data
  exclusions <- x1[date <= start_date]
  
  if (grepl("feb_w1", x)) {
    exclusions <- exclusions[x2, on = c("description"), exclude := i.feb_w1]
  }
  if (grepl("feb_w2", x)) {
    exclusions <- exclusions[x2, on = c("description"), exclude := i.feb_w2]
  }
  if (grepl("tax_w1", x)) {
    exclusions <- exclusions[x2, on = c("description"), exclude := i.tax_w1]
  }
  if (grepl("tax_w2", x)) {
    exclusions <- exclusions[x2, on = c("description"), exclude := i.tax_w2]
  }
  if (grepl("sep_w1", x)) {
    exclusions <- exclusions[x2, on = c("description"), exclude := i.sep_w1]
  }
  if (grepl("sep_w2", x)) {
    exclusions <- exclusions[x2, on = c("description"), exclude := i.sep_w2]
  }
  if (grepl("eoy_w1", x)) {
    exclusions <- exclusions[x2, on = c("description"), exclude := i.eoy_w1]
  }
  if (grepl("eoy_w2", x)) {
    exclusions <- exclusions[x2, on = c("description"), exclude := i.eoy_w1]
  }
  exclusions[exclude == "Y", unique(urn)]
}

remove_exclusions <- function(x = index, x1 = donor_segments, x2 = donor_exclusions) {
  x1[[x]][urn %in% x2[[x]], remove := "Y"]
}
   
create_urn <- function(x) {
  get(x)[, urn := as.character(`18 Digit Contact ID`)]
}

allocate_trans <- function(x, start_date) {
  x2 <- get(gsub("w1", "tx", x))

  x2[get(x), on = "urn", wave := ifelse(i.date_w2 < date, "w2", NA)]
  x2[is.na(wave), wave := "w1"]
  x2[, income_week := floor(as.numeric(date - start_date) / 7)]
  x3 <- dcast(x2[, .(gifts = .N, amount = sum(amount)), .(urn, wave)],
        urn ~ wave, value.var = c("gifts", "amount"), fill = 0)
  x3[, `:=`(total_gifts = gifts_w1 + gifts_w2,
            total_amount = amount_w1 + amount_w2)]
  x3
}

join_trans <- function(x) {
  tx <- appeal_trans
  mb <- get(x)

  mb[tx, on = "urn",
     `:=`(gifts_w1 = i.gifts_w1,
          amount_w1 = i.amount_w1,
          # gifts_w2 = i.gifts_w2,
          # amount_w2 = i.amount_w2,
          gifts_total = i.gifts_total,
          amount_total = i.amount_total)]
  
  mb[,`:=`(gifts_w1 = ifelse(is.na(gifts_w1), 0, gifts_w1),
           amount_w1 = ifelse(is.na(amount_w1), 0, amount_w1),
           # gifts_w2 = ifelse(is.na(gifts_w2), 0, gifts_w2),
           # amount_w2 = ifelse(is.na(amount_w2), 0, amount_w2),
           gifts_total = ifelse(is.na(gifts_total), 0, gifts_total),
           amount_total = ifelse(is.na(amount_total), 0, amount_total))]
}

join_segments <- function(x) {
  segs <- appeal_segments[[x]]
  mb <- get(x)

  mb[segs, on = "urn",
     `:=`(donor_type = i.donor_type,
          segment_code = i.segment_code,
          appeal = i.appeal)]
}

calc_ask_multipliers <- function(x) {
  message(x)
  get(x)[`Last Cash Gift` !=0, `:=`(mult_ask1 = Ask1/`Last Cash Gift`,
                                    mult_ask2 = Ask2/`Last Cash Gift`,
                                    mult_ask3 = Ask3/`Last Cash Gift`)]
}


calc_hag <- function(x, 
                     classification_codes = NULL, 
                     channel_codes = NULL,
                     months = 24, 
                     refresh_date, 
                     prefix = "") {
  
  if (length(classification_codes) > 0) {
    x <- x[classification_code %in% classification_codes]
  }
  
  if (length(channel_codes) > 0) {
    x <- x[channel_code %in% channel_codes]
  }
  
  if (prefix != "")
    prefix <- paste0(prefix, "_")
  
  hag_date <- lubridate::`%m-%`(as.Date(refresh_date), months(months))
  x <- x[date >= hag_date]
  setkey(x, amount)
  
  x[x[, .I[.N], urn]$V1,
    setNames(list(date, amount),
             paste0(prefix, c("hag_date", "hag_amount"))), urn]
  
}